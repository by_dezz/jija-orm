from __future__ import annotations

import abc
import asyncio
import dataclasses
import pathlib
import typing

from jija_orm import config
from test_orm import models
from test_orm.models import Test, Jija

config.JijaORM(
    project_dir=pathlib.Path(__file__).parent.parent,
    connection=config.Connection(
        host='localhost',
        port=5422,
        user='postgres',
        database='postgres',
        password='0000'
    ),
    apps=(
        config.App(name='test_orm'),
    )
)


from jija_orm.executors import operators, functions, processors
from jija_orm import executors

# a = executors.Executor(
#     models.Jija,
#     templates.MultipleInsert(
#         [
#             {'id': 2, 'suka': True, 'asdasd': 2},
#             {'id': 3, 'suka': True, 'asdasd': 3},
#             {'id': 4, 'suka': True, 'asdasd': 4},
#             {'id': 5, 'suka': True, 'asdasd': 5}
#         ]
#     )
# )

from jija_orm.executors import operators
b = executors.Executor(
    models.Jija,
    operators.Select('id', ff='suka')
)

# from test_orm import main

migration_id_selector = executors.Executor(
    'jija_orm',
    operators.Select(id=functions.Max('id')),
    use_model=False
)


from jija_orm.migrator import selectors


async def main():
    # a = await selectors.columns_selector.execute(table='blad')
    # print(a)
    # print(await models.Blad.where(d=22))
    # print(f)
    # print(jija, jija.ads)
    #
    # jija.ads = True
    # await jija.save()
    # # print(jija, jija.ads)
    # print(await models.Jija.insert(suka=True, blad=1))
    # asd = executors.Executor(
    #     models.Juja,
    #     operators.Select(),
    #     # operators.Where(id=99999),
    # # operators.Select('id', ff='suka'),
    #     processors.Single()
    # )
    #
    #
    # qu = await asd.execute()
    # print(qu)
    # print(qu[0].jija.operators[1].objects[0].objects)
    # print(await qu[0].jija)

    # a = (await b.execute())[0].
    # a.blad += 1

    # b = a.update(ads=False)
    # await b
    # print(await a)
    # c[0].
    # Jija.manager.model.
    # c[0].
    # c[0]
    # c[0].

    # print(Jija.manager.model.)
    # await a
    #
    from jija_orm.migrator import migrator
    # a = migrator.Migrator()
    await migrator.Migrator.migrate()
    await migrator.Migrator.update()

loop = asyncio.new_event_loop().run_until_complete(main())
# print(a.query)
# print(a.sync_execute())
