from jija_orm import fields, models
from jija_orm.executors import query


class Test(models.Model):
    a = fields.BooleanField()
    aa = fields.BooleanField()


class Jija(models.Model):
    jija = query.RelatedManager('test_orm.Juja')

    suka = fields.BooleanField(null=False)
    blad = fields.IntegerField(null=False)


class Juja(models.Model):
    d = fields.CharField(max_length=1)
    aad = fields.CharField(max_length=1, null=True)
    dd = fields.CharField(max_length=1, null=False)
    f = fields.BooleanField()
    pizda = fields.ForeignKey(relation_to=Test, on_delete=fields.OnDelete.SET_NULL)


class Blad(models.Model):
    d = fields.ForeignKey(relation_to=Jija, on_delete=fields.OnDelete.CASCADE)
