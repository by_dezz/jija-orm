import unittest
from jija_orm.migrator import templates


class TestFieldMigrationOperators(unittest.TestCase):
    @staticmethod
    def __create_field(action, attrs=None, values=None):
        return templates.FieldMigration(
            'field', action,
            list(map(lambda item: templates.AttributeMigration(item[1], values[item[0]] if values else 1),
                     enumerate(attrs))) if attrs else []
        )

    def test_sqwash_create(self):
        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.CREATE, ['a', 'b'], [1, 2]),
            self.__create_field(templates.Action.CREATE, ['b', 'c'], [3, 4])
        )
        self.assertEqual(self.__create_field(templates.Action.CREATE, ['a', 'b', 'c'], [1, 3, 4]), result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.CREATE, ['a', 'b']),
            self.__create_field(templates.Action.DROP, [])
        )
        self.assertIsNone(result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.CREATE, ['a', 'b'], [1, 2]),
            self.__create_field(templates.Action.ALTER, ['b'], [3])
        )
        self.assertEqual(self.__create_field(templates.Action.CREATE, ['a', 'b'], [1, 3]), result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.CREATE, ['a', 'b'], [1, 2]),
            None
        )
        self.assertEqual(self.__create_field(templates.Action.CREATE, ['a', 'b'], [1, 2]), result)

    def test_sqwash_drop(self):
        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.DROP),
            self.__create_field(templates.Action.CREATE)
        )
        self.assertEqual(self.__create_field(templates.Action.CREATE), result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.DROP), self.__create_field(templates.Action.DROP))
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.DROP), self.__create_field(templates.Action.DROP))
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.DROP),
            None
        )
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

    def test_sqwash_alter(self):
        self.assertRaises(
            AttributeError,
            lambda: templates.FieldMigration.sqwash(
                self.__create_field(templates.Action.ALTER),
                self.__create_field(templates.Action.CREATE)
            )
        )

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.ALTER),
            self.__create_field(templates.Action.DROP)
        )
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        result = templates.FieldMigration.sqwash(
            self.__create_field(templates.Action.ALTER, ['a', 'b', 'c'], [1, 2, 3]),
            self.__create_field(templates.Action.ALTER, ['b', 'c', 'd'], [2, 2, 4])
        )
        self.assertEqual(self.__create_field(templates.Action.ALTER, ['a', 'b', 'c', 'd'], [1, 2, 2, 4]), result)

        result = templates.FieldMigration.sqwash(self.__create_field(templates.Action.ALTER), None)
        self.assertEqual(self.__create_field(templates.Action.ALTER), result)

    def test_sqwash_none(self):
        result = templates.FieldMigration.sqwash(None, self.__create_field(templates.Action.CREATE))
        self.assertEqual(self.__create_field(templates.Action.CREATE), result)

        self.assertRaises(
            AttributeError, lambda: templates.FieldMigration.sqwash(None, self.__create_field(templates.Action.DROP)))

        self.assertRaises(
            AttributeError, lambda: templates.FieldMigration.sqwash(None, self.__create_field(templates.Action.ALTER)))

        result = templates.FieldMigration.sqwash(None, None)
        self.assertIsNone(result)

    def test_difference_create(self):
        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.CREATE, ['a', 'b', 'c'], [1, 2, 3]),
            self.__create_field(templates.Action.CREATE, ['a', 'b'], [1, 3])
        )
        self.assertEqual(self.__create_field(templates.Action.ALTER, ['b', 'c'], [2, 3]), result)

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.CREATE, ['a']),
            self.__create_field(templates.Action.CREATE, ['a'])
        )
        self.assertIsNone(result)

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.CREATE, ['a', 'b']),
            self.__create_field(templates.Action.DROP, [])
        )
        self.assertEqual(self.__create_field(templates.Action.CREATE, ['a', 'b']), result)

        self.assertRaises(
            AttributeError,
            lambda: templates.FieldMigration.difference(
                self.__create_field(templates.Action.CREATE),
                self.__create_field(templates.Action.ALTER)
            )
        )

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.CREATE, ['a'], [1]),
            None
        )
        self.assertEqual(self.__create_field(templates.Action.CREATE, ['a'], [1]), result)

    def test_difference_drop(self):
        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.DROP),
            self.__create_field(templates.Action.CREATE)
        )
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.DROP),
            self.__create_field(templates.Action.DROP)
        )
        self.assertIsNone(result)

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.DROP),
            self.__create_field(templates.Action.ALTER)
        )
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        self.assertRaises(
            AttributeError,
            lambda: templates.FieldMigration.difference(self.__create_field(templates.Action.DROP), None)
        )

    def test_difference_alter(self):
        self.assertRaises(
            AttributeError,
            lambda: templates.FieldMigration.difference(
                self.__create_field(templates.Action.ALTER),
                self.__create_field(templates.Action.CREATE)
            )
        )

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.ALTER, ['a', 'b', 'c'], [1, 2, 3]),
            self.__create_field(templates.Action.DROP)
        )
        self.assertEqual(self.__create_field(templates.Action.ALTER, ['a', 'b', 'c'], [1, 2, 3]), result)

        result = templates.FieldMigration.difference(
            self.__create_field(templates.Action.ALTER, ['a', 'b', 'c'], [1, 2, 3]),
            self.__create_field(templates.Action.ALTER, ['b', 'c', 'd'], [2, 2, 4])
        )
        self.assertEqual(self.__create_field(templates.Action.ALTER, ['a', 'c'], [1, 3]), result)

        self.assertRaises(
            AttributeError,
            lambda: templates.FieldMigration.difference(self.__create_field(templates.Action.ALTER), None)
        )

    def test_difference_none(self):
        result = templates.FieldMigration.difference(None, self.__create_field(templates.Action.CREATE))
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        result = templates.FieldMigration.difference(None, self.__create_field(templates.Action.DROP))
        self.assertIsNone(result)

        result = templates.FieldMigration.difference(None, self.__create_field(templates.Action.ALTER))
        self.assertEqual(self.__create_field(templates.Action.DROP), result)

        result = templates.FieldMigration.difference(None, None)
        self.assertIsNone(result)


class TestAttributeMigrationOperators(unittest.TestCase):
    @staticmethod
    def __create_attribute(value):
        return templates.AttributeMigration('attr', value)

    def test_sqwash(self):
        result = templates.AttributeMigration.sqwash(self.__create_attribute(1), self.__create_attribute(1))
        self.assertEqual(self.__create_attribute(1), result)

        result = templates.AttributeMigration.sqwash(self.__create_attribute(1), self.__create_attribute(2))
        self.assertEqual(self.__create_attribute(2), result)

        result = templates.AttributeMigration.sqwash(None, self.__create_attribute(1))
        self.assertEqual(self.__create_attribute(1), result)

        result = templates.AttributeMigration.sqwash(self.__create_attribute(1), None)
        self.assertEqual(self.__create_attribute(1), result)

    def test_differences(self):
        result = templates.AttributeMigration.difference(self.__create_attribute(1), self.__create_attribute(1))
        self.assertEqual(None, result)

        result = templates.AttributeMigration.difference(self.__create_attribute(2), self.__create_attribute(1))
        a = self.__create_attribute(2)
        self.assertEqual(self.__create_attribute(2), result)

        result = templates.AttributeMigration.difference(self.__create_attribute(1), self.__create_attribute(2))
        self.assertEqual(self.__create_attribute(1), result)

        result = templates.AttributeMigration.difference(None, self.__create_attribute(1))
        self.assertEqual(None, result)

        result = templates.AttributeMigration.difference(self.__create_attribute(1), None)
        self.assertEqual(self.__create_attribute(1), result)
