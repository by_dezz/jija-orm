from jija_orm import models
from jija_orm import fields


class Test(models.Model):
    integer = fields.IntegerField()
    related = fields.ForeignKey(relation_to=models.Model, on_delete=fields.OnDelete.CASCADE)
    second_related = fields.ForeignKey(relation_to=models.Model, on_delete=fields.OnDelete.CASCADE)
    char = fields.CharField(max_length=16)
