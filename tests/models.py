import unittest

from jija_orm import models, fields
from tests_utils import models as test_models


class Model(unittest.TestCase):
    def test_get_fields(self):
        self.assertEqual(
            {'id': fields.IntegerField(pk=True)},
            models.Model.get_fields()
        )

        assert_data = {
            'id': fields.IntegerField(pk=True),
            'integer': fields.IntegerField(),
            'char': fields.CharField(max_length=16)
        }

        result_data = test_models.Test.get_fields()

        self.assertEqual(assert_data, result_data)
        self.assertEqual(list(assert_data.keys()), list(result_data.keys()))

    def test_get_constraints(self):
        self.assertEqual({}, models.Model.get_constraints())

        assert_data = {
            'related': fields.ForeignKey(relation_to=models.Model, on_delete=fields.OnDelete.CASCADE),
            'second_related': fields.ForeignKey(relation_to=models.Model, on_delete=fields.OnDelete.CASCADE)
        }
        result_data = test_models.Test.get_constraints()

        self.assertEqual(assert_data, result_data)
        self.assertEqual(list(assert_data.keys()), list(result_data.keys()))
