import unittest

from jija_orm.executors import operators, sql_object
from jija_orm import executors

from tests_utils.models import Test


class TestExecutor(unittest.TestCase):
    def test_init(self):
        executor = executors.Executor(Test)
        self.assertEqual(Test, executor.model)

        executor = executors.Executor(Test, use_model=False)
        self.assertEqual(Test.get_name(), executor.model)

        executor = executors.Executor('test')
        self.assertEqual('test', executor.model)

    def test_get_params(self):
        select = operators.Select()
        where = operators.Where(integer=sql_object.Param('name'))
        executor = executors.Executor(Test, select, where)

        self.assertEqual({'name'}, executor.params)

    def test_parse_operators(self):
        select_template = operators.Select()
        where_template = operators.Where(integer=sql_object.Param('name'))

        executor = executors.Executor(Test, select_template, where_template)

        self.assertEqual({'name'}, executor.params)
        self.assertEqual(Test, select_template.model)
        self.assertEqual(Test, where_template.model)

        select_template = operators.Select()
        where_template = operators.Where(a=sql_object.Param('name'))

        executor = executors.Executor('test', select_template, where_template, use_model=False)

        self.assertEqual({'name'}, executor.params)
        self.assertEqual('test', select_template.model)
        self.assertEqual('test', where_template.model)

    def test_generate_query(self):
        executor = executors.Executor(Test, operators.Select(), operators.Where(id=1))
        # executor.operators = {'val1': '1', 'val2': '2', 'val3': '3'}

        self.assertEqual('\n'.join(['select * from "test"', 'where "id" = 1']), executor.query)

    # def test_sync_execute(self): TODO
    #     pass
    #
    # def test_execute(self): TODO
    #     pass

    def test_serialize(self):
        data = [
            {'id': 1, 'integer': 1, 'related': 3, 'char': '2'},
            {'id': 2, 'integer': 10, 'related': 30, 'char': '20'},
            {'id': 3, 'integer': 100, 'related': 300, 'char': '200'},
        ]

        executor = executors.Executor(Test)
        self.assertEqual([Test(**item) for item in data], executor._Executor__serialize(data))

        executor = executors.Executor(Test, use_model=False)
        self.assertEqual(data, executor._Executor__serialize(data))
