import unittest
from unittest import mock

from jija_orm import exceptions
from jija_orm.executors import sql_object
from tests_utils.models import Test


class TestDecorators(unittest.TestCase):
    def test_calculated_decorator(self):
        test_class = mock.MagicMock()
        test_class.calculated = True

        def method(this, *args, **kwargs):
            self.assertEqual(test_class, this)
            return 'success'

        test_class.protected_method = sql_object.calculated_decorator(method)
        test_class.protected_method(test_class)

        self.assertEqual('success', test_class.protected_method(test_class))
        test_class.calculated = False
        self.assertRaises(ValueError, lambda: test_class.protected_method(test_class))

    def test_validated_decorator(self):
        test_class = mock.MagicMock()
        test_class.validated = True

        def method(this, *args, **kwargs):
            self.assertEqual(test_class, this)
            return 'success'

        test_class.protected_method = sql_object.validated_decorator(method)
        test_class.protected_method(test_class)

        self.assertEqual('success', test_class.protected_method(test_class))
        test_class.validated = False
        self.assertRaises(ValueError, lambda: test_class.protected_method(test_class))


class TestSimpleSQLObject(unittest.TestCase):
    class TestSqlObject(sql_object.SimpleSQLObject):
        def get_query(self):
            pass

    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_init(self):
        obj = sql_object.SimpleSQLObject()
        self.assertEqual(None, obj.model)
        self.assertEqual(None, obj._SimpleSQLObject__query)
        self.assertEqual(False, obj.calculated)

    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_prepare(self):
        obj = sql_object.SimpleSQLObject()
        obj.prepare(Test)
        self.assertEqual(Test, obj.model)

        obj = sql_object.SimpleSQLObject()
        obj.prepare('test')
        self.assertEqual('test', obj.model)

    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_calculate(self):
        obj = sql_object.SimpleSQLObject()
        obj.get_query = mock.Mock()
        obj.get_query.return_value = 'test'

        self.assertEqual('test', obj.calculate())
        self.assertEqual('test', obj.query)
        self.assertTrue(obj.calculated)
        self.assertEqual(1, obj.get_query.call_count)

    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_get_name(self):
        obj = sql_object.SimpleSQLObject()
        self.assertEqual('simplesqlobject', obj.get_name())

        obj = self.TestSqlObject()
        self.assertEqual('testsqlobject', obj.get_name())

    def test_correct_name(self):
        name = sql_object.SimpleSQLObject.correct_name('test')
        self.assertEqual('"test"', name)

        name = sql_object.SimpleSQLObject.correct_name("test")
        self.assertEqual('"test"', name)

        name = sql_object.SimpleSQLObject.correct_name('te.st')
        self.assertEqual('"te"."st"', name)

        name = sql_object.SimpleSQLObject.correct_name('"te"."st"')
        self.assertEqual('"te"."st"', name)

    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_eq(self):
        self.assertFalse(self.TestSqlObject() == sql_object.SimpleSQLObject())

        first = sql_object.SimpleSQLObject()
        second = sql_object.SimpleSQLObject()
        self.assertTrue(first == second)
        self.assertTrue(second == first)

        first.prepare(Test)
        self.assertFalse(first == second)
        self.assertFalse(second == first)

        second.prepare(Test)
        self.assertTrue(first == second)
        self.assertTrue(second == first)

        first.get_query = mock.Mock()
        first.get_query.return_value = 1
        first.calculate()

        self.assertFalse(first == second)
        self.assertFalse(second == first)

        second.get_query = mock.Mock()
        second.get_query.return_value = 1
        second.calculate()

        self.assertTrue(first == second)
        self.assertTrue(second == first)

    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_calculated_eq(self):
        first = sql_object.SimpleSQLObject()
        second = sql_object.SimpleSQLObject()

        first.get_query = mock.Mock()
        first.get_query.return_value = 1
        first.calculate()

        second.get_query = mock.Mock()
        second.get_query.return_value = 2
        second.calculate()

        self.assertFalse(first == second)
        self.assertFalse(second == first)


class TestValidationSQLObject(unittest.TestCase):
    @mock.patch.multiple(sql_object.ValidationSQLObject, __abstractmethods__=set())
    def test_init(self):
        obj = sql_object.ValidationSQLObject()
        self.assertFalse(obj.validated)

    @mock.patch.multiple(sql_object.ValidationSQLObject, __abstractmethods__=set())
    def test_prepare(self):
        obj = sql_object.ValidationSQLObject()
        obj.validate = mock.Mock()

        obj.prepare(Test)
        self.assertTrue(obj.validated)


class SQLObject(unittest.TestCase):
    @mock.patch.multiple(sql_object.SimpleSQLObject, __abstractmethods__=set())
    def test_init(self):
        obj = sql_object.SQLObject([])
        self.assertEqual([], obj.objects)
        self.assertEqual([], obj._SQLObject__objects_query)

        obj = sql_object.SQLObject({'first': sql_object.SimpleSQLObject(), 'second': sql_object.SimpleSQLObject()})
        self.assertEqual({'first': sql_object.SimpleSQLObject(), 'second': sql_object.SimpleSQLObject()}, obj.objects)
        self.assertEqual({}, obj._SQLObject__objects_query)

    def test_prepare(self):
        # Test list
        first = mock.MagicMock()
        second = mock.MagicMock()

        obj = sql_object.SQLObject([first, second])
        obj.prepare(Test)

        self.assertEqual(Test, obj.model)
        self.assertEqual(1, first.prepare.call_count)
        self.assertEqual(1, second.prepare.call_count)

        # Test dict
        first = mock.MagicMock()
        second = mock.MagicMock()

        obj = sql_object.SQLObject({'first': first, 'second': second})
        obj.prepare(Test)

        self.assertEqual(Test, obj.model)
        self.assertEqual(1, first.prepare.call_count)
        self.assertEqual(1, second.prepare.call_count)

    @mock.patch.multiple(sql_object.SQLObject, __abstractmethods__=set())
    def test_calculate(self):
        first = mock.MagicMock()
        first.calculate.return_value = 'Bye'
        second = mock.MagicMock()
        second.calculate.return_value = 'object'

        obj = sql_object.SQLObject([first, second])
        obj.prepare(Test)
        obj.get_query = lambda: 'Hi test'

        self.assertEqual('Hi test', obj.calculate())
        self.assertEqual(['Bye', 'object'], obj.objects_query)
        self.assertEqual(1, first.calculate.call_count)
        self.assertEqual(1, second.calculate.call_count)

        first = mock.MagicMock()
        first.calculate.return_value = 'Bye'
        second = mock.MagicMock()
        second.calculate.return_value = 'object'

        obj = sql_object.SQLObject({'first': first, 'second': second})
        obj.prepare(Test)
        obj.get_query = lambda: 'Hi test'

        self.assertEqual('Hi test', obj.calculate())
        self.assertEqual({'first': 'Bye', 'second': 'object'}, obj.objects_query)
        self.assertEqual(1, first.calculate.call_count)
        self.assertEqual(1, second.calculate.call_count)


class TestTemplateSqlObjectMixin(unittest.TestCase):
    def test_init(self):
        obj = sql_object.TemplateSqlObject([])
        self.assertIsNone(obj._TemplateSqlObject__template_params)

    def test_get_template_params(self):
        obj = sql_object.TemplateSqlObject([])
        self.assertRaises(NotImplementedError, obj.get_template_params)

    def test_get_query(self):
        obj = sql_object.TemplateSqlObject([])

        obj.TEMPLATE = '{first} {second}'
        obj.get_template_params = lambda: {'first': 'Hi', 'second': 'test'}

        self.assertEqual('Hi test', obj.get_query())


class TestValue(unittest.TestCase):
    def test_init(self):
        obj = sql_object.Value(1)
        self.assertEqual(1, obj.raw_value)
        self.assertIsNone(obj._Value__value)
        self.assertIsNone(obj.field)

        obj = sql_object.Value(1, 'a')
        self.assertEqual('a', obj.field)

    def test_validate(self):
        obj = sql_object.Value(None)
        obj.prepare(Test)
        self.assertEqual('null', obj.value)

        obj = sql_object.Value('value')
        obj.prepare('test')
        self.assertEqual("'value'", obj.value)

    def test_get_validator(self):
        from jija_orm import fields

        obj = sql_object.Value(1)
        obj.prepare(Test)
        self.assertEqual(fields.IntegerField, obj.get_validator())

        obj = sql_object.Value(None, 'char')
        obj.prepare(Test)
        self.assertEqual(Test.char, obj.get_validator())

        obj = sql_object.Value(None, 'none')
        obj.prepare(Test)
        self.assertRaises(exceptions.TemplateAttributeError, obj.get_validator)

        obj = sql_object.Value(False, 'char')
        obj.prepare('test')
        self.assertEqual(fields.BooleanField, obj.get_validator())

    def test_get_query(self):
        obj = sql_object.Value(1)
        obj.prepare(Test)
        self.assertEqual(1, obj.get_query())

    def test_eq(self):
        self.assertTrue(sql_object.Value(1) == sql_object.Value(1))
        self.assertTrue(sql_object.Value(1, 'a') == sql_object.Value(1, 'a'))

        self.assertFalse(sql_object.Value(1) == sql_object.Value(2))
        self.assertFalse(sql_object.Value(1, 'a') == sql_object.Value(1, 'b'))
        self.assertFalse(sql_object.Value(1, 'a') == sql_object.Value(2, 'a'))

        first = sql_object.Value(1)
        first.prepare(Test)
        first.calculate()

        second = sql_object.Value(1)
        second.prepare(Test)
        second.calculate()

        third = sql_object.Value(2)
        third.prepare(Test)
        third.calculate()

        self.assertTrue(first == second)
        self.assertTrue(second == first)

        self.assertFalse(first == third)
        self.assertFalse(third == first)

        self.assertFalse(second == third)
        self.assertFalse(third == second)


class TestField(unittest.TestCase):
    def test_init(self):
        obj = sql_object.Field('name')
        self.assertEqual('name', obj.raw_name)
        self.assertIsNone(obj._Field__name)

    def test_validate(self):
        obj = sql_object.Field('related')
        obj.prepare(Test)
        self.assertEqual('"related_id"', obj.name)

        obj = sql_object.Field('integer')
        obj.prepare(Test)
        self.assertEqual('"integer"', obj.name)

        obj = sql_object.Field('none')
        self.assertRaises(exceptions.TemplateAttributeError, lambda: obj.prepare(Test))

        obj = sql_object.Field('related')
        obj.prepare('test')
        self.assertEqual('"related"', obj.name)

        obj = sql_object.Field('integer')
        obj.prepare('test')
        self.assertEqual('"integer"', obj.name)

        obj = sql_object.Field('none')
        obj.prepare('test')
        self.assertEqual('"none"', obj.name)

    def test_get_query(self):
        obj = sql_object.Field('field')
        obj.prepare('test')
        self.assertEqual('"field"', obj.get_query())

    def test_eq(self):
        self.assertTrue(sql_object.Field('a') == sql_object.Field('a'))
        self.assertFalse(sql_object.Field('a') == sql_object.Field('b'))

        first = sql_object.Field('a')
        first.prepare('test')
        first.calculate()

        second = sql_object.Field('a')
        second.prepare('test')
        second.calculate()

        third = sql_object.Field('b')
        third.prepare('test')
        third.calculate()

        self.assertTrue(first == second)
        self.assertTrue(second == first)

        self.assertFalse(first == third)
        self.assertFalse(third == first)

        self.assertFalse(second == third)
        self.assertFalse(third == second)
