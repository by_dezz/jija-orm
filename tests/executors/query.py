import unittest
from unittest import mock

from jija_orm.executors import query, operators
from tests_utils.models import Test


class TestQueryManager(unittest.TestCase):
    def test_init(self):
        with mock.patch('jija_orm.executors.query.QueryManager') as query_manager_mock:
            query.QueryManager(Test)
            self.assertEqual(1, query_manager_mock.call_count)

    def test_fix_operators(self):
        self.assertEqual([], query.QueryManager(Test).fix_operators(None))
        self.assertEqual([], query.QueryManager(Test).fix_operators([]))

        self.assertEqual(
            [operators.Delete(), operators.Where()],
            query.QueryManager(Test).fix_operators([
                operators.Select(),
                operators.Delete(),
                operators.Where()
            ])
        )

        with mock.patch('jija_orm.executors.operators.Where') as where_mock:
            where_mock.COMPARABLE = True
            self.assertRaises(
                NotImplementedError,
                lambda: query.QueryManager(Test).fix_operators([operators.Where(), operators.Where()])
            )

        # self.assertEqual( TODO
        #     [operators.Where(), operators.Where()],
        #     query.QueryManager(Test).fix_operators([
        #         operators.Where(),
        #         operators.Where()
        #     ])
        # )

    def test_select(self):
        self.assertEqual([operators.Select('a')], query.QueryManager(Test).select('a').operators)

    def test_update(self):
        self.assertEqual([operators.Update(a=1)], query.QueryManager(Test).update(a=1).operators)

    def test_insert(self):
        self.assertEqual([operators.Insert(a=1)], query.QueryManager(Test).insert(a=1).operators)

    def test_multiple_insert(self):
        self.assertEqual(
            [operators.MultipleInsert([{'a': 1}])],
            query.QueryManager(Test).multiple_insert([{'a': 1}]).operators
        )

    def test_where(self):
        empty_query = query.QueryManager(Test)
        self.assertEqual(
            [operators.Select(), operators.Where(a=1)],
            empty_query.where(a=1).operators
        )

        delete_query = empty_query.delete()
        self.assertEqual(
            [operators.Delete(), operators.Where(a=1)],
            delete_query.where(a=1).operators
        )


class TestRelatedManager(unittest.TestCase):
    def test_get_model(self):
        manager = query.RelatedManager(Test)
        self.assertEqual(Test, manager.get_model())

        manager = query.RelatedManager(mock.Mock)
        self.assertRaises(TypeError, manager.get_model)

        with mock.patch('jija_orm.config.JijaORM') as JijaORM_mock:
            JijaORM_mock.get_model.return_value = 'test_model'
            manager = query.RelatedManager('model_name')

            self.assertEqual('test_model', manager.get_model())
            self.assertEqual(1, JijaORM_mock.get_model.call_count)

        manager = query.RelatedManager(mock.Mock())
        self.assertRaises(TypeError, manager.get_model)

    def test_get_field(self):
        manager = query.RelatedManager(None, 'field_name')
        self.assertEqual('field_name', manager.get_field(Test, Test))

        model_a_mock = mock.MagicMock()
        model_b_mock = mock.MagicMock()

        model_a_mock.get_constraints.return_value = {
            'a': self.__create_relation_field_mock(model_b_mock),
            'b': self.__create_relation_field_mock(model_b_mock)
        }

        manager = query.RelatedManager(mock.Mock)
        self.assertRaises(
            ValueError,
            lambda mbm=model_b_mock, mam=model_a_mock: manager.get_field(mbm, mam)
        )

        model_a_mock.get_constraints.return_value = {}
        self.assertRaises(
            ValueError,
            lambda mbm=model_b_mock, mam=model_a_mock: manager.get_field(mbm, mam)
        )

        model_a_mock.get_constraints.return_value = {'a': self.__create_relation_field_mock(model_b_mock)}
        self.assertEqual('a', manager.get_field(model_b_mock, model_a_mock))

    @staticmethod
    def __create_relation_field_mock(relation_to_class):
        field_mock = mock.Mock()
        field_mock.relation_to_class = relation_to_class
        return field_mock

    def test_get_manager(self):  # TODO
        pass
