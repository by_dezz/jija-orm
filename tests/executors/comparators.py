import unittest

from jija_orm.executors import comparators, sql_object


class TestComparator(unittest.TestCase):
    def test_init(self):
        comparator = comparators.Comparator('a', 1)
        self.assertEqual({
            'field': sql_object.Field('a'),
            'value': sql_object.Value(1, 'a')
        }, comparator.objects)

        comparator = comparators.Comparator('a', sql_object.Value(1), g=sql_object.Value(2))
        self.assertEqual({
            'field': sql_object.Field('a'),
            'value': sql_object.Value(1),
            'g': sql_object.Value(2)
        }, comparator.objects)

    def get_template_params(self):
        comparator = comparators.Comparator('a', 1)
        comparator.prepare('test')
        comparator.calculate()

        comparator.OPERATOR = 'not bool'
        comparator.BOOL_OPERATOR = 'bool'

        self.assertEqual({
            'field': comparator.objects['field'].query,
            'value': comparator.objects['value'].query,
            'operator': comparator.OPERATOR
        }, comparator.get_template_params())

        comparator = comparators.Comparator('a', True)
        comparator.prepare('test')
        comparator.calculate()

        comparator.OPERATOR = 'not bool'
        comparator.BOOL_OPERATOR = 'bool'

        self.assertEqual({
            'field': comparator.objects['field'].query,
            'value': comparator.objects['value'].query,
            'operator': comparator.BOOL_OPERATOR
        }, comparator.get_template_params())

    def test_bool_protect(self):
        self.assertTrue(comparators.Comparator.bool_protect(sql_object.Value(True)))
        self.assertFalse(comparators.Comparator.bool_protect(sql_object.SQLObject([])))

    def test_calculate(self):
        comparator = comparators.NumericComparator('field', 1)
        comparator.prepare('table')
        comparator.OPERATOR = '<operator>'
        self.assertEqual('"field" <operator> 1', comparator.calculate())


class TestNumericComparator(unittest.TestCase):
    def test_calculate(self):
        comparator = comparators.NumericComparator('field', 1)
        comparator.OPERATOR = '<operator>'
        comparator.prepare('table')
        self.assertEqual('"field" <operator> 1', comparator.calculate())

        comparator = comparators.NumericComparator('field', 1, equal=True)
        comparator.OPERATOR = '<operator>'
        comparator.prepare('table')
        self.assertEqual('"field" <operator>= 1', comparator.calculate())


class TestNot(unittest.TestCase):
    def test_calculate(self):
        comparator = comparators.Not('test', 1)
        comparator.prepare('table')
        self.assertEqual('"test" != 1', comparator.calculate())

        comparator = comparators.Not('field', True)
        comparator.prepare('table')
        self.assertEqual('"field" is not true', comparator.calculate())


class TestEqual(unittest.TestCase):
    def test_calculate(self):
        comparator = comparators.Equal('test', 1)
        comparator.prepare('table')
        self.assertEqual('"test" = 1', comparator.calculate())

        comparator = comparators.Equal('field', True)
        comparator.prepare('table')
        self.assertEqual('"field" is true', comparator.calculate())
