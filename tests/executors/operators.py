import unittest
from unittest import mock

from jija_orm.executors import operators, sql_object, functions, comparators
from tests_utils.models import Test


# class TestOperator(unittest.TestCase):
#     def test_get_table_name(self):
#         operator = operators.Operator()
#         operator.prepare(Test)
#
#         self.assertEqual('"test"', operator.get_table_name())
#
#         operator = operators.Operator()
#         operator.prepare('my_model')
#
#         self.assertEqual('"my_model"', operator.get_table_name())


class TestSelect(unittest.TestCase):
    def test_init(self):
        operator = operators.Select()
        self.assertEqual([], operator.objects)

        operator = operators.Select('id', distinct=True)
        self.assertEqual([sql_object.Field('id')], operator.objects)

        operator = operators.Select('id', 'integer', custom='related', b=functions.Max('id'))
        self.assertEqual([
            sql_object.Field('id'),
            sql_object.Field('integer'),
            functions.Alias('related', 'custom'),
            functions.Alias(functions.Max('id'), 'b')
            ],
            operator.objects
        )

    def test_get_template_params(self):
        operator = operators.Select()
        operator.prepare(Test)
        operator.calculate()

        self.assertEqual({
            'fields': '*',
            'distinct': '',
            'table_name': '"test"'
        }, operator.get_template_params())

        operator = operators.Select('id', distinct=True)
        operator.prepare(Test)
        operator.calculate()

        self.assertEqual({
            'fields': '"id"',
            'distinct': 'distinct ',
            'table_name': '"test"'
        }, operator.get_template_params())

        operator = operators.Select('id', 'integer', custom='related')
        operator.prepare(Test)
        operator.calculate()
        self.assertEqual({
            'fields': '"id", "integer", "related_id" as "custom"',
            'distinct': '',
            'table_name': '"test"'
        }, operator.get_template_params())

    def test_calculate(self):
        operator = operators.Select()
        operator.prepare(Test)
        self.assertEqual('select * from "test"', operator.calculate())

        operator = operators.Select('id', distinct=True)
        operator.prepare(Test)
        self.assertEqual('select distinct "id" from "test"', operator.calculate())


class TestInsert(unittest.TestCase):
    def test_init(self):
        operator = operators.Insert()
        self.assertEqual({
            'fields': sql_object.SQLObject([]),
            'values': sql_object.SQLObject({})
        }, operator.objects)

        operator = operators.Insert(related=1, char='2')
        self.assertEqual({
            'fields': sql_object.SQLObject([sql_object.Field('related'), sql_object.Field('char')]),
            'values': sql_object.SQLObject({
                'related': sql_object.Value(1, 'related'),
                'char': sql_object.Value('2', 'char')
            })
        }, operator.objects)

    def test_get_template_params(self):
        operator = operators.Insert()
        operator.prepare(Test)
        operator.calculate()

        self.assertEqual({
            'fields': '',
            'values': '',
            'table_name': '"test"'
        }, operator.get_template_params())

        operator = operators.Insert(related=1, char='2')
        operator.prepare('custom_model')
        operator.calculate()

        self.assertEqual({
            'fields': '"related", "char"',
            'values': '1, \'2\'',
            'table_name': '"custom_model"'
        }, operator.get_template_params())

        operator = operators.Insert(id=1, integer=2)
        operator.prepare(Test)
        operator.calculate()
        self.assertEqual({
            'fields': '"id", "integer"',
            'values': '1, 2',
            'table_name': '"test"'
        }, operator.get_template_params())

    def test_calculate(self):
        operator = operators.Insert(id=1, integer=2)
        operator.prepare(Test)
        self.assertEqual('insert into "test" ("id", "integer") values (1, 2) returning *', operator.calculate())

        operator = operators.Insert(related=1, char='2')
        operator.prepare('custom_model')
        self.assertEqual(
            'insert into "custom_model" ("related", "char") values (1, \'2\') returning *', operator.calculate())


class TestInsertRow(unittest.TestCase):
    def test_init(self):
        operator = operators.InsertRow(['a', 'b'], {'a': 1, 'b': 2})
        self.assertEqual([sql_object.Value(1, 'a'), sql_object.Value(2, 'b')], operator.objects[0].objects)

        operator = operators.InsertRow(['a'], {'a': 1, 'b': 2})
        self.assertEqual([sql_object.Value(1, 'a')], operator.objects[0].objects)

        operator = operators.InsertRow(['a', 'b'], {'a': 1})
        self.assertEqual([sql_object.Value(1, 'a'), sql_object.Value(None, 'b')], operator.objects[0].objects)

        operator = operators.InsertRow(['a', 'b'], {'a': 1, 'b': functions.Max('test')})
        self.assertEqual([sql_object.Value(1, 'a'), functions.Max('test')], operator.objects[0].objects)

    def test_get_template_params(self):
        operator = operators.InsertRow(['a', 'b'], {'a': 1, 'b': 2})
        operator.prepare('test')
        operator.calculate()
        self.assertEqual({
            'values': '1, 2'
        }, operator.get_template_params())

    def test_calculate(self):
        operator = operators.InsertRow(['a', 'b'], {'a': 1, 'b': functions.Max('tf')})
        operator.prepare('test')
        self.assertEqual(
            '(1, max("tf"))',
            operator.calculate()
        )


class TestMultipleInsert(unittest.TestCase):
    rows_a = []

    rows_b = [{'integer': 1, 'related': 3}, {'integer': 3}, {'integer': 4, 'related': 3}, {'related': 3}]

    rows_c = [{'integer': 1, 'char': '2'}, {'integer': 1, 'related': 3},
              {'char': '2', 'related': 3}, {'integer': 4, 'char': '2', 'related': 3}]

    def test_init(self):
        for rows in (self.rows_a, self.rows_b, self.rows_c):
            operator = operators.MultipleInsert(rows)
            fields = operator.get_fields(rows)
            self.assertEqual(operator.objects, {
                'fields': sql_object.SQLObject([sql_object.Field(field) for field in fields]),
                'rows': sql_object.SQLObject([operators.InsertRow(fields, row) for row in rows])
            })

    def test_get_fields(self):
        self.assertEqual([], operators.MultipleInsert.get_fields(self.rows_a))

        self.assertEqual(
            sorted(['integer', 'related']),
            operators.MultipleInsert.get_fields(self.rows_b)
        )

        self.assertEqual(
            sorted(['integer', 'related', 'char']),
            operators.MultipleInsert.get_fields(self.rows_c)
        )

    def test_calculate(self):
        operator = operators.MultipleInsert(self.rows_c)
        operator.get_fields = mock.Mock()
        operator.get_fields.return_value = ['integer', 'related', 'char']
        operator.prepare(Test)

        self.assertEqual(
            'insert into "test" ("char", "integer", "related_id") '
            'values (\'2\', 1, null), (null, 1, 3), (\'2\', null, 3), (\'2\', 4, 3) '
            'returning *',
            operator.calculate()
        )


class TestUpdateUnit(unittest.TestCase):
    def test_init(self):
        operator = operators.UpdateUnit('a', 1)
        self.assertEqual({
            'field': sql_object.Field('a'),
            'value': sql_object.Value(1, 'a')
        }, operator.objects)

        operator = operators.UpdateUnit('a', functions.Max('field'))
        self.assertEqual({
            'field': sql_object.Field('a'),
            'value': functions.Max('field')
        }, operator.objects)

    def test_get_template_params(self):
        operator = operators.UpdateUnit('a', 1)
        operator.prepare('test')
        operator.calculate()

        self.assertEqual({
            'field': '"a"',
            'value': 1,
        }, operator.get_template_params())

    def test_calculate(self):
        operator = operators.UpdateUnit('a', 1)
        operator.prepare('test')

        self.assertEqual(
            '"a" = 1',
            operator.calculate()
        )


class TestUpdate(unittest.TestCase):
    def test_init(self):
        operator = operators.Update(a=1, b=2, c=functions.Max('field'))
        self.assertEqual([
            operators.UpdateUnit('a', 1),
            operators.UpdateUnit('b', 2),
            operators.UpdateUnit('c', functions.Max('field')),
        ], operator.objects)

    def test_get_template_params(self):
        operator = operators.Update(a=1, b=2)
        operator.prepare('test')
        operator.calculate()

        self.assertEqual({
            'params': '"a" = 1, "b" = 2',
            'table_name': '"test"'
        }, operator.get_template_params())

    def test_calculate(self):
        operator = operators.Update(a=1, b=2, c=3)
        operator.prepare('test')

        self.assertEqual(
            'update "test" set "a" = 1, "b" = 2, "c" = 3',
            operator.calculate()
        )


class TestDelete(unittest.TestCase):
    def test_init(self):
        operator = operators.Delete()
        self.assertEqual([], operator.objects)

    def test_calculate(self):
        operator = operators.Delete()
        operator.prepare('test')

        self.assertEqual('delete from "test"', operator.calculate())


class TestWhere(unittest.TestCase):
    def test_init(self):
        operator = operators.Where()
        self.assertEqual([], operator.objects)

        operator = operators.Where(comparators.Lower('c', 4), a=1)
        self.assertEqual([comparators.Lower('c', 4), comparators.Equal('a', 1)], operator.objects)

        self.assertRaises(TypeError, lambda: operators.Where(1))

    def test_get_templates_params(self):
        operator = operators.Where(comparators.Lower('c', 4), a=1, c=True)
        operator.prepare('test')
        operator.calculate()

        self.assertEqual({
            'expressions': '"c" < 4 and "a" = 1 and "c" is true'
        }, operator.get_template_params())

    def test_calculate(self):
        operator = operators.Where(comparators.Lower('c', 4), a=functions.Max('c'), c=True)
        operator.prepare('test')

        self.assertEqual('where "c" < 4 and "a" = max("c") and "c" is true', operator.calculate())

